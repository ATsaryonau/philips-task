create table department (
	id int not null primary key,
	d_name varchar(128)
);

create table student (
	id int not null primary key,
	d_id int references department(id),
	s_name varchar(128)
);

create table test (
	id int not null primary key,
	d_id int references department(id)
);

create table test_score (
	t_id int references test(id),
	s_id int references student(id),
	score int default 0
);

