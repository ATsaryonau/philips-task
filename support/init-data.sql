insert into department values (1, 'Department 1'), (2, 'Department 2'), (3, 'Department 3');
insert into student values (1, 1, 'Student 1_D1'), (2, 1, 'Student 2_D1'), (3, 1, 'Student 3_D1'),
						   (4, 2, 'Student 1_D2'), (5, 2, 'Student 2_D2'), (6, 3, 'Student 1_D3'), (7, 3, 'Student 2_D3');
insert into test values (1, 1), (2, 2), (3, 3);
insert into test_score values (1, 1, 86), (1, 2, 88), (1, 3, 92), (2, 4, 90), (2, 5, 32), (3, 6, 80), (3, 7, 70);