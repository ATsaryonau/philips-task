select d.d_name, s.s_name, ts.score from test_score ts
join (
	select t_id as test_id, max(score) as score from test_score
	group by t_id
) mts on mts.score = ts.score and mts.test_id = ts.t_id
join student s on ts.s_id = s.id
join department d on d.id = s.d_id
order by d.d_name
