# Task description
_Students are assigned to some departments. Each department provides a test to its
students. Find the students with the highest scores for test in each department._
# Additional info
I have provided some additional scripts for DB initialization in the **/support** folder.